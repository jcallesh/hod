import numpy as np
import pyhod.hodfunc as hf
import pyhod.read as read
import pyhod.utils as utils
import config


def header(params = None):
    print('\n================================================================================')
    print('                    RUNNING GALAXY CATALOG GENERATOR                          ')
    print('================================================================================\n')
    if params['iRSD'] == 0:
        print('Computing in realspace.')
    elif params['iRSD'] < 4:
        print('Computing in Redshift Space Distortion in Plane Parallel Approximation.')
    else:
        print('Computing in Redshift Space Distortion for Wide Angle Observer.')
    if params['zerr'] == 1:
        print('redshift error type: spectroscopic.')
    elif params['zerr'] == 2:
        print('redshift error type: photometric.')
    print('--------------------------------------------------------------------------------')

def main(params = None):
    
    haloCat = read.readcats(params = params)  ## units: mass: [Msun/h], pos:[Mpc/h], vel:[km/s]
    #haloCat = haloCat[(haloCat[:,0] > par.mmin) & (haloCat[:,0] < par.mmax)]
    print('Loaded %d halos, in the mass rage (%.2e,%.2e).\n' %(len(haloCat[:,0]),haloCat[:,0].min(),haloCat[:,0].max()))

    label = np.array(['logMmin','sigmalogM','logM0','alpha','logM1'])
    outlabel = np.array(['_fid','_logMmin_p','_sigmalogM_p','_logM0_p','_alpha_p','_logM1_p',\
                                '_logMmin_m','_sigmalogM_m','_logM0_m','_alpha_m','_logM1_m'])

    step_vec = np.zeros((params['nsteps']*10+1,5))  ##initiate with fiducial
    Ntot = 10*params['nsteps']+1
    for i in range(1,Ntot):
        step_vec[i,i%5-1]= (1+i//(Ntot/params['nsteps']))*(-1)**((i-1)//5)

    for i,step in enumerate(step_vec):
        if i>0: print('\nComputing step %i: %s %+i\n----------------'%(i,*label[step!=0],step[step!=0]))
        HOD_par = ( params['logMmin']   + step[0]*params['st_logMmin'],\
                    params['sigmalogM'] + step[1]*params['st_sigmalogM'],\
                    params['logM0']     + step[2]*params['st_logM0'],\
                    params['alpha']     + step[3]*params['st_alpha'],\
                    params['logM1']     + step[4]*params['st_logM1'] )

        print('HOD pararameters:', HOD_par)
        centrals, satellites = hf.cent_sat(haloCat,*HOD_par, params = params)
        galaxy_pos, galaxy_vel, galaxy_type = hf.sample_catalog(haloCat, centrals, satellites, params = params)

        output = params['outfile']
        output = output.replace('HOD0','HOD%s'%outlabel[i])
        print('\nNow Saving at:\n    --->OUTPUT FILE:%s\n'%output)
        np.savez_compressed(output, pos=galaxy_pos, vel =galaxy_vel, gtype = galaxy_type, hodpar= HOD_par)

if __name__=="__main__":

    '''Load User Input'''
    params = config.input_params()
    print(params)
    '''header'''
    header(params=params)
    
    '''MAIN'''
    main(params=params)

    print('\n***program run successfully.***')